describe("My First Test", function() {
  it("identify yourself", function() {
    cy.visit("http://tzookb.track.dev.cordial.io/dev-test/crisis.html");

    cy.server();
    cy.route("GET", /contact.php/).as("identify");

    cy.get("#contactID").type("tzookb@gmail.com");
    cy.contains("Fire cordial.identify").click();

    cy.wait("@identify").then(function(xhr) {
      let body = xhr.response.body;

      expect(body.cID).to.be.a("string");
      expect(xhr.status).to.equal(200);
    });
  });

  it("identify yourself", function() {
    cy.visit("http://tzookb.track.dev.cordial.io/dev-test/crisis.html");

    cy.server();
    cy.route("GET", /track.php/).as("event");

    cy.get("#eventName").type("someEvent");
    cy.get("#track").click();

    cy.wait("@event").then(function(xhr) {
      let body = xhr.response.body;

      expect(body.success).to.be.a("boolean");
      expect(xhr.status).to.equal(200);
    });
  });
});
