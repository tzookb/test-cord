describe("My First Test", function() {
  it("identify yourself", function() {
    cy.viewport(1200, 1000)
    cy.visit("https://tzookb.admin.dev.cordial.io/");

    cy.server();
    cy.route("GET", /api\/dashboard/).as("dashboard");

    cy.get("#username").type("tzookb@gmail.com");
    cy.wait(1200);
    cy.get("#password").type("Cor2042Dial");
    cy.wait(1200);
    cy.get("button[data-btn='login']").click();
    
    cy.wait("@dashboard");
    cy.get(".profile_menu").click();
    cy.wait(1200);
    cy.get("[data-class='integrations']").click();

    cy.get(".cl-vnavigation > li:nth-child(2)").click();
    cy.wait(1600);
    cy.get(".cl-vnavigation > li:nth-child(2) li:nth-child(2)").click();
  
    // cy.wait("@identify").then(function(xhr) {
    //   let body = xhr.response.body;

    //   expect(body.cID).to.be.a("string");
    //   expect(xhr.status).to.equal(200);
    // });
  });
});