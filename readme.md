### How to

- clone repo
- enter folder
- npm install

now you have 2 options for testing:
- test and develop with UI
- test in terminal

##### for testing with ui:
- npm run dev

a new cypresjs windo will open with list of tests,
you can now run the tests and edit them in your editor.


##### for testing on CI:
- npm run test

tests will run on command line, and a video will be created just in case.
Results will be displayed.


